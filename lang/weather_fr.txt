cond_200 = "orages avec lègère pluie"
cond_201 = "orages avec pluie"
cond_202 = "orages avec forte pluie"
cond_210 = "légers orages"
cond_211 = "orages"
cond_212 = "forts orages"
cond_221 = "orages irréguliers"
cond_230 = "orages avec légère bruine"
cond_231 = "orages avec bruine"
cond_232 = "orages avec une bruine lourde"
cond_300 = "légère bruine"
cond_301 = "bruine"
cond_302 = "forte bruine"
cond_310 = "légère bruine, pluie"
cond_311 = "bruine, pluie"
cond_312 = "forte bruine, pluie"
cond_313 = "averses, pluie et bruine"
cond_314 = "fortes averse, pluie et bruine"
cond_321 = "averses, bruine"
cond_500 = "légère pluie"
cond_501 = "pluie modérée"
cond_502 = "forte pluie"
cond_503 = "très forte pluie"
cond_504 = "pluie extrême"
cond_511 = "pluie verglaçante"
cond_520 = "légères averses, pluie"
cond_521 = "averses, pluie"
cond_522 = "fortes averses, pluie"
cond_531 = "averses irrégulières, pluie"
cond_600 = "légère neige"
cond_601 = "neige"
cond_602 = "forte neige"
cond_611 = "neige fondue"
cond_612 = "averses de neige fondue"
cond_615 = "légère pluie et neige"
cond_616 = "pluie et neige"
cond_620 = "légères averses de neige"
cond_621 = "averses de neige"
cond_622 = "fortes averses de neige"
cond_701 = "brouillard"
cond_711 = "fumée"
cond_721 = "brume"
cond_731 = "sable, tourbillons de poussière"
cond_741 = "brouillard"
cond_751 = "sable"
cond_761 = "poussière"
cond_762 = "cendre volcanique"
cond_771 = "bourrasques"
cond_781 = "tornade"
cond_800 = "ciel dégagé"
cond_801 = "peu nuageux"
cond_802 = "nuages épars"
cond_803 = "nuageux"
cond_804 = "très nuageux"














































