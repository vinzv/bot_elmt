cAF  = "Afghanistan"
cAX  = "Ålandinseln"
cAL  = "Albanien"
cDZ  = "Algerien"
cAS  = "Amerikanisch-Samoa"
cAD  = "Andorra"
cAO  = "Angola"
cAI  = "Anguilla"
cAQ  = "Antarktis"
cAG  = "Antigua und Barbuda"
cAR  = "Argentinien"
cAM  = "Armenien"
cAW  = "Aruba"
cAU  = "Australien"
cAT  = "Österreich"
cAZ  = "Aserbaidschan"
cBS  = "Bahamas"
cBH  = "Bahrain"
cBD  = "Bangladesch"
cBB  = "Barbados"
cBY  = "Weißrussland"
cBE  = "Belgien"
cBZ  = "Belize"
cBJ  = "Benin"
cBM  = "Bermudas"
cBT  = "Bhutan"
cBO  = "Bolivien, Plurinationaler Staat von"
cBQ  = "Bonaire, Sint Eustatius und Saba"
cBA  = "Bosnien und Herzegowina"
cBW  = "Botswana"
cBV  = "Bouvet-Insel"
cBR  = "Brasilien"
cIO  = "Britisches Territorium im Indischen Ozean"
cBN  = "Brunei Darussalam"
cBG  = "Bulgarien"
cBF  = "Burkina Faso"
cBI  = "Burundi"
cCV  = "Cabo Verde"
cKH  = "Kambodscha"
cCM  = "Kamerun"
cCA  = "Kanada"
cKY  = "Kaimaninseln"
cCF  = "Zentralafrikanische Republik"
cTD  = "Tschad"
cCL  = "Chile"
cCN  = "China"
cCX  = "Weihnachtsinsel"
cCC  = "Cocos (Keeling) Inseln"
cCO  = "Kolumbien"
cKM  = "Komoren"
cCG  = "Kongo"
cCD  = "Kongo, die Demokratische Republik Kongo"
cCK  = "Cookinseln"
cCR  = "Costa Rica"
cCI  = "Côte d'Ivoire"
cHR  = "Kroatien"
cCU  = "Kuba"
cCW  = "Curaçao"
cCY  = "Zypern"
cCZ  = "Tschechien"
cDK  = "Dänemark"
cDJ  = "Dschibuti"
cDM  = "Dominica"
cDO  = "Dominikanische Republik"
cEC  = "Ecuador"
cEG  = "Ägypten"
cSV  = "El Salvador"
cGQ  = "Äquatorialguinea"
cER  = "Eritrea"
cEE  = "Estland"
cSZ  = "Eswatini"
MEZ  = "Äthiopien"
cFK  = "Falklandinseln (Malvinas)"
cFO  = "Färöer-Inseln"
cFJ  = "Fidschi"
cFI  = "Finnland"
cFR  = "Frankreich"
cGF  = "Französisch-Guayana"
cPF  = "Französisch-Polynesien"
cTF  = "Französische Südgebiete"
cGA  = "Gabun"
cGM  = "Gambia"
cGE  = "Georgien"
cDE  = "Deutschland"
cGH  = "Ghana"
cGI  = "Gibraltar"
cGR  = "Griechenland"
cGL  = "Grönland"
cGD  = "Grenada"
cGP  = "Guadeloupe"
cGU  = "Guam"
cGT  = "Guatemala"
cGG  = "Guernsey"
cGN  = "Guinea"
cGW  = "Guinea-Bissau"
cGY  = "Guyana"
cHT  = "Haiti"
cHM  = "Heard und McDonaldinseln"
cVA  = "Heiliger Stuhl"
cHN  = "Honduras"
cHK  = "Hongkong"
cHU  = "Ungarn"
cIS  = "Island"
cIN  = "Indien"
cID  = "Indonesien"
cIR  = "Iran, Islamische Republik"
cIQ  = "Irak"
cIE  = "Irland"
cIM  = "Isle of Man"
cIL  = "Israel"
cIT  = "Italien"
cJM  = "Jamaika"
cJP  = "Japan"
cJE  = "Jersey"
cJO  = "Jordanien"
cKZ  = "Kasachstan"
cKE  = "Kenia"
cKI  = "Kiribati"
cKP  = "Korea, Demokratische Volksrepublik Korea"
cKR  = "Korea, Republik Korea"
cKW  = "Kuwait"
cKG  = "Kirgisistan"
cLA  = "Demokratische Volksrepublik Laos"
cLV  = "Lettland"
cLB  = "Libanon"
cLS  = "Lesotho"
cLR  = "Liberia"
cLY  = "Libyen"
cLI  = "Liechtenstein"
cLT  = "Litauen"
cLU  = "Luxemburg"
cMO  = "Macao"
cMK  = "Mazedonien, die ehemalige jugoslawische Republik"
cMG  = "Madagaskar"
cMW  = "Malawi"
cMY  = "Malaysia"
cMV  = "Malediven"
cML  = "Mali"
cMT  = "Malta"
cMH  = "Marshallinseln"
cMQ  = "Martinique"
cMR  = "Mauretanien"
cMU  = "Mauritius"
cYT  = "Mayotte"
cMX  = "Mexiko"
cFM  = "Mikronesien, Föderierte Staaten von"
cMD  = "Moldawien, Republik"
cMC  = "Monaco"
cMN  = "Mongolei"
cME  = "Montenegro"
cMS  = "Montserrat"
cMA  = "Marokko"
cMZ  = "Mosambik"
cMM  = "Myanmar"
cNA  = "Namibia"
cNR  = "Nauru"
cNP  = "Nepal"
cNL  = "Niederlande"
cNC  = "Neukaledonien"
cNZ  = "Neuseeland"
cNI  = "Nicaragua"
cNE  = "Niger"
cNG  = "Nigeria"
cNU  = "Niue"
cNF  = "Norfolkinsel"
cMP  = "Nördliche Marianeninseln"
cNO  = "Norwegen"
cOM  = "Oman"
cPK  = "Pakistan"
cPW  = "Palau"
cPS  = "Palästina, Staat"
cPA  = "Panama"
cPG  = "Papua-Neuguinea"
cPY  = "Paraguay"
cPE  = "Peru"
cPH  = "Philippinen"
cPN  = "Pitcairn"
cPL  = "Polen"
cPT  = "Portugal"
cPR  = "Puerto Rico"
cQA  = "Katar"
cRE  = "Réunion"
cRO  = "Rumänien"
cRU  = "Russische Föderation"
cRW  = "Ruanda"
cBL  = "Saint Barthélemy"
cSH  = "Saint Helena, Ascension and Tristan da Cunha"
cKN  = "Saint Kitts and Nevis"
cLC  = "Saint Lucia"
cMF  = "Saint Martin (französischer Teil)"
cPM  = "Saint Pierre und Miquelon"
cVC  = "St Vincent und die Grenadinen"
cWS  = "Samoa"
cSM  = "San Marino"
cST  = "Sao Tome und Principe"
cSA  = "Saudi-Arabien"
cSN  = "Senegal"
cRS  = "Serbien"
cSC  = "Seychellen"
cSL  = "Sierra Leone"
cSG  = "Singapur"
cSX  = "Sint Maarten (niederländischer Teil)"
cSK  = "Slowakei"
cSI  = "Slowenien"
cSB  = "Salomonen"
cSO  = "Somalia"
cZA  = "Südafrika"
cGS  = "Südgeorgien und die südlichen Sandwichinseln"
cSS  = "Südsudan"
cES  = "Spanien"
cLK  = "Sri Lanka"
cSD  = "Sudan"
cSR  = "Suriname"
cSJ  = "Svalbard und Jan Mayen"
cSE  = "Schweden"
cCH  = "Schweiz"
cSY  = "Syrische Arabische Republik"
cTW  = "Taiwan, Provinz China"
cTJ  = "Tadschikistan"
cTZ  = "Tansania, Vereinigte Republik"
cTH  = "Thailand"
cTL  = "Timor-Leste"
cTG  = "Togo"
cTK  = "Tokelau"
cTO  = "Tonga"
cTT  = "Trinidad und Tobago"
cTN  = "Tunesien"
cTR  = "Türkei"
cTM  = "Turkmenistan"
cTC  = "Turks- und Caicosinseln"
cTV  = "Tuvalu"
cUG  = "Uganda"
cUA  = "Ukraine"
cAE  = "Vereinigte Arabische Emirate"
cGB  = "Vereinigtes Königreich Großbritannien und Nordirland"
cUM  = "United States Minor Outlying Islands"
cUS  = "Vereinigte Staaten von Amerika"
cUY  = "Uruguay"
cUZ  = "Usbekistan"
cVU  = "Vanuatu"
cVE  = "Venezuela, Bolivarische Republik"
cVN  = "Vietnamesisch"
cVG  = "Jungferninseln, Britisch"
cVI  = "Virgin Islands, US"
cWF  = "Wallis und Futuna"
cEH  = "Westsahara"
cYE  = "Jemen"
cZM  = "Sambia"
cZW  = "Simbabwe"
