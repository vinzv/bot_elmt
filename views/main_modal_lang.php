	<div class="modal-dialog  ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?=t('Language')?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php
			if(isset($err)) { ?>
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$err?>
			</div>
		<?php }
			if(isset($wrn)) { ?>
			<div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$wrn?>
			</div>
		<?php }
			if(isset($msg)) { ?>
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$msg?>
			</div>
		<?php }	?>

			
			<div class="modal-footer">
				<button type="button" id="btnOK" class="btn btn-primary" data-dismiss="modal"><?=t('OK')?></button>
			</div>
</form>
		</div>
	</div>

<script>
	// display result in modal
	// modalize_form('#form-form');
	$('#btnOK').click(function(){
		location.reload(true);
	});
</script>
