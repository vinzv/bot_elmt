</div>

<footer class="footer">
	<div class="container">
		<a href="https://framagit.org/jpfox/bot_elmt">Bot Elmt</a> is under <a href="https://www.gnu.org/licenses/agpl-3.0.html">GNU/AGPLv3</a>,
		use data from <a href="https://openweathermap.org">OpenWeatherMap.org</a> under <a href="https://opendatacommons.org/licenses/odbl/">ODbL</a></small>
		and icons from <a href="https://www.adilade.fr/blog/icones-meteo-adilade-weather-icons-set/">Adilade</a> under <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-by-sa</a>
	</div>
</footer>

<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="js/datatables.min.js"></script>-->
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/jquery.form.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="js/js.cookie.min.js"></script>
<script type="text/javascript" src="js/bfwk.js"></script>

<script>
	if(typeof Intl !== 'undefined') {
		var browser_tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
		Cookies.set('browser_tz', browser_tz);
	}
</script>
</body>
</html>
