<?php

class log
{
	static public function addlog($strMessage, $bDisplay=false)
	{
		// vérification du message
		if (trim($strMessage) == "")
			return false;

		// vérification de la présence du répertoire log
		if (!file_exists(dirname(APP_UPLOADS_LOGS)))
			@mkdir(dirname(APP_UPLOADS_LOGS));

		// établissement du nom du fichier
		$strFileName = "logs-" . date("Y-m-d") . ".txt";

		// écriture du log
		if (($fHandle = fopen(APP_UPLOADS_LOGS . $strFileName, "a+")) !== false) {
			fwrite($fHandle, date("H:i:s: ") . $_SERVER["REMOTE_ADDR"] . ": $strMessage\n");
			fclose($fHandle);
		}
		
		if($bDisplay)
			echo date("Y-m-d H:i:s: ") . ": $strMessage\n";

		// retour ok
		return true;
	}
	
	/**
	 * message bandeau
	 * $fileid : idclient ou "exp-".idexpert 
	 * $strMessage : message à afficher
	 * source : source de 1 à 3 pour avoir des messages venant de plusieurs sources (appel en cours, rechargement...)
	 */
	static public function setajaxmessage($fileid, $strMessage, $source=1) {
		// vérification du message
		if (trim($strMessage) == "")
			return false;
		$prefix = '';

		// vérification de la présence du répertoire log
		if (!file_exists(dirname(APP_UPLOADS_MSG)))
			@mkdir(dirname(APP_UPLOADS_MSG));

		// établissement du nom du fichier
		$strFileName = "msg-" . $fileid . '-S' . $source . ".txt";
		if(substr($strMessage,0,1)=='!') {
			$posBracket = strpos($strMessage, ']');
			if($posBracket===false)
				$posBracket = 0;
			$prefix = substr($strMessage,0,$posBracket+1);
			$strMessage = substr($strMessage,$posBracket+1);
		}
		// écriture du log
		if (@file_put_contents(APP_UPLOADS_MSG . $strFileName,  $prefix . date("H:i:s : ") . $strMessage) !== false) {
			return true;
		}

		// retour ok
		return false;
	} 
}
